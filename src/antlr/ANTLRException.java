package antlr;

/* ANTLR Translator Generator
 * Project led by Terence Parr at http://www.jGuru.com
 * Software rights: http://www.antlr.org/RIGHTS.html
 *
 * $Id: ANTLRException.java,v 1.1 2004/03/13 10:34:45 elinares Exp $
 */
public class ANTLRException extends Exception {
	public ANTLRException() {
		super();
	}

	public ANTLRException(String s) {
		super(s);
	}
}
