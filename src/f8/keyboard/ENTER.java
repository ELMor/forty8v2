/*
 * Created on 22-jul-2003
 *
 
 
 */
package f8.keyboard;

import f8.CL;
import f8.CalcGUI;
import f8.commands.stk.DUP;
import f8.exceptions.F8Exception;
import f8.exceptions.ICalcErr;

/**
 * @author elinares
 * 
 * 
 * 
 */
public final class ENTER extends Tecla {
	public ENTER() {
		super("ENTER");
	}

	public void pressed(CalcGUI cg) throws F8Exception {
		CL cl = cg.getCalcLogica();

		if (cg.getEditField().length() > 0) {
			cl.enter(cg.getEditField());
			cg.refresh(true);
		} else {
			if (cl.check(1)) {
				if (!cg.getEditField().equals(""))
					cl.enter(cg.getEditField());
				new DUP().exec();
				cg.refresh(true);
			} else {
				cg.output(ICalcErr.TooFewArguments);
			}
		}
	}
}
