/*
 * Created on 12-jun-2003
 *
 
 
 */
package f8.keyboard.hp48;

import f8.CL;
import f8.CalcGUI;
import f8.Ref;
import f8.exceptions.DebugHaltException;
import f8.exceptions.F8Exception;
import f8.exceptions.ICalcErr;
import f8.keyboard.Alpha;
import f8.keyboard.CF;
import f8.keyboard.Menu;
import f8.keyboard.ShiftKey;
import f8.keyboard.TeclaVirtual;
import f8.keyboard.WithEnter;
import f8.keyboard.hp48.groups.Keyboard;
import f8.keyboard.hp48.menu.MTH;
import f8.keyboard.hp48.menu.PRG;

/**
 * @author elinares
 * 
 * 
 * 
 */
public final class HP48 implements ICalcErr, CF {
	private static int alp = 0;

	private static boolean pgmMode = false;

	private static int shi = 0;

	private static int upMenuPage = 0;

	private Menu doMenu;

	private Menu miMenu;

	private Menu upMenu;

	public HP48() {
		doMenu = Keyboard.normDown();
		miMenu = Keyboard.normMiddle();
		upMenu = new MTH();
	}

	public void resetUpMenuPage() {
		upMenuPage = 0;
	}

	public void nxtUpMenuPage() {
		int ums = upMenu.size();
		if (ums > 6) {
			int topPage = (ums / 6) + (((ums % 6) > 0) ? 0 : (-1));
			if (upMenuPage >= topPage) {
				upMenuPage = 0;
			} else {
				upMenuPage++;
			}
		}
	}

	public void prevUpMenuPage() {
		if (upMenu.size() > 6) {
			if (upMenuPage == 0) {
				upMenuPage = (upMenu.size() - 1) / 6;
			} else {
				upMenuPage--;
			}
		}
	}

	public int getAlphaMode() {
		return alp;
	}

	public String[] getDoStringArray() {
		return doMenu.getStringArray(0, 20);
	}

	public String[] getMiStringArray() {
		return miMenu.getStringArray(0, 24);
	}

	public boolean getPgmMode() {
		return pgmMode;
	}

	public int getShiftMode() {
		return shi;
	}

	public String[] getUpStringArray() {
		return upMenu.getStringArray(upMenuPage * 6, 6);
	}

	public void key(int i) {
		TeclaVirtual vk;

		if (i < 6) {
			vk = upMenu.getContentAt((upMenuPage * 6) + i);
		} else if ((i > 5) && (i < 29)) {
			vk = miMenu.getContentAt(i - 6);
		} else {
			vk = doMenu.getContentAt(i - 29);
		}
		press(vk);
	}

	public void press(TeclaVirtual vk) {
		try {
			vk.pressed(Ref.ref);
		} catch (DebugHaltException h) {
			CalcGUI cg = Ref.ref;
			CL cl = cg.getCalcLogica();
			cg.setMenu(1, PRG.ctrlMenu(), null, null);
			cg.refresh(true);
			cg.temporaryLabels(null, cl.getProcDebug().nextCommand());
		} catch (F8Exception e) {
			Ref.ref.getCalcLogica().error(e);
		}

		// Comprobamos estado de Shift y Alpha status
		if (vk instanceof Alpha || vk instanceof ShiftKey) {
			return;
		}

		if (getAlphaMode() == 1) {
			setAlphaMode(0);
			Ref.ref.setMenu(0, null, Keyboard.normMiddle(), Keyboard.normDown());
		}

		if (getShiftMode() > 0) {
			setShiftMode(0);
			Ref.ref.setMenu(0, null, Keyboard.normMiddle(), Keyboard.normDown());
		}
	}

	public void setAlphaMode(int amod) {
		alp = amod;
		Ref.ref.refreshStatusLabels();
	}

	public void setPgmMode(boolean em) {
		pgmMode = em;
	}

	public void setShiftMode(int smode) {
		shi = smode;
		Ref.ref.refreshStatusLabels();
	}

	/**
	 * @return
	 */
	public Menu getDoMenu() {
		return doMenu;
	}

	/**
	 * @return
	 */
	public Menu getMiMenu() {
		return miMenu;
	}

	/**
	 * @return
	 */
	public Menu getUpMenu() {
		return upMenu;
	}

	/**
	 * @param menu
	 */
	public void setDoMenu(Menu menu) {
		doMenu = menu;
	}

	/**
	 * @param menu
	 */
	public void setMiMenu(Menu menu) {
		miMenu = menu;
	}

	/**
	 * @param menu
	 */
	public void setUpMenu(Menu menu) {
		upMenu = menu;
		resetUpMenuPage();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.ui.CF#getUpIsDirectory()
	 */
	public boolean[] getUpIsDirectory() {
		boolean[] ret = new boolean[6];
		for (int i = upMenuPage * 6; i < ((upMenuPage * 6) + 6); i++) {
			ret[i - (upMenuPage * 6)] = upMenu.getContentAt(i).isDir();
		}
		return ret;
	}

	public boolean[] getUpVarSolver() {
		boolean[] col = new boolean[6];
		for (int i = upMenuPage * 6; i < ((upMenuPage * 6) + 6); i++) {
			TeclaVirtual m = upMenu.getContentAt(i);
			col[i - (upMenuPage * 6)] = m.isVarSolver();
		}
		return col;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.ui.CF#getUpIsOn()
	 */
	public boolean[] getUpIsOn() {
		boolean[] ret = new boolean[6];
		for (int i = upMenuPage * 6; i < ((upMenuPage * 6) + 6); i++) {
			Object tec = upMenu.getContentAt(i);
			if (tec instanceof WithEnter) {
				ret[i - (upMenuPage * 6)] = ((WithEnter) tec).isChecked();
			} else {
				ret[i - (upMenuPage * 6)] = false;
			}
		}
		return ret;
	}

	/**
	 * @return
	 */
	public int getUpMenuPage() {
		return upMenuPage;
	}

	/**
	 * @param i
	 */
	public void setUpMenuPage(int i) {
		upMenuPage = i;
	}

}
