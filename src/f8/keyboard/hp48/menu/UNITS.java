/*
 * Created on 22-jul-2003
 *
 
 
 */
package f8.keyboard.hp48.menu;

import f8.keyboard.Menu;
import f8.keyboard.TeclaVirtual;
import f8.keyboard.UnitMenu;

/**
 * @author elinares
 * 
 * 
 * 
 */
public final class UNITS extends Menu {
	public UNITS() {
		super("UNITS");
		appendKey(lengMenu());
		appendKey(areaMenu());
		appendKey(volMenu());
		appendKey(timeMenu());
		appendKey(speedMenu());
		appendKey(massMenu());
		appendKey(forceMenu());
		appendKey(enrgMenu());
		appendKey(powrMenu());
		appendKey(pressMenu());
		appendKey(tempMenu());
		appendKey(elecMenu());
		appendKey(anglMenu());
		appendKey(lightMenu());
		appendKey(radMenu());
		appendKey(viscMenu());
	}

	private TeclaVirtual lengMenu() {
		Menu m = new Menu("LENG");
		m.appendKey(new UnitMenu("m"));
		m.appendKey(new UnitMenu("cm"));
		m.appendKey(new UnitMenu("mm"));
		m.appendKey(new UnitMenu("yd"));
		m.appendKey(new UnitMenu("ft"));
		m.appendKey(new UnitMenu("in"));
		m.appendKey(new UnitMenu("mpc"));
		m.appendKey(new UnitMenu("pc"));
		m.appendKey(new UnitMenu("lyr"));
		m.appendKey(new UnitMenu("au"));
		m.appendKey(new UnitMenu("km"));
		m.appendKey(new UnitMenu("mi"));
		m.appendKey(new UnitMenu("nmi"));
		m.appendKey(new UnitMenu("mius"));
		m.appendKey(new UnitMenu("chain"));
		m.appendKey(new UnitMenu("rd"));
		m.appendKey(new UnitMenu("fath"));
		m.appendKey(new UnitMenu("ftus"));
		m.appendKey(new UnitMenu("mil"));
		m.appendKey(new UnitMenu("pico"));
		m.appendKey(new UnitMenu("a"));
		m.appendKey(new UnitMenu("fermi"));
		return m;
	}

	private TeclaVirtual areaMenu() {
		Menu m = new Menu("AREA");
		m.appendKey(new UnitMenu("m^2"));
		m.appendKey(new UnitMenu("cm^2"));
		m.appendKey(new UnitMenu("b"));
		m.appendKey(new UnitMenu("yd^2"));
		m.appendKey(new UnitMenu("ft^2"));
		m.appendKey(new UnitMenu("in^2"));
		m.appendKey(new UnitMenu("km^2"));
		m.appendKey(new UnitMenu("ha"));
		m.appendKey(new UnitMenu("a"));
		m.appendKey(new UnitMenu("mi^2"));
		m.appendKey(new UnitMenu("mius^2"));
		m.appendKey(new UnitMenu("acre"));
		return m;
	}

	private TeclaVirtual volMenu() {
		Menu m = new Menu("VOL");
		m.appendKey(new UnitMenu("m^3"));
		m.appendKey(new UnitMenu("st"));
		m.appendKey(new UnitMenu("cm^3"));
		m.appendKey(new UnitMenu("yd^3"));
		m.appendKey(new UnitMenu("ft^3"));
		m.appendKey(new UnitMenu("in^3"));
		m.appendKey(new UnitMenu("l"));
		m.appendKey(new UnitMenu("galu"));
		m.appendKey(new UnitMenu("galc"));
		m.appendKey(new UnitMenu("gal"));
		m.appendKey(new UnitMenu("qt"));
		m.appendKey(new UnitMenu("pt"));
		m.appendKey(new UnitMenu("ml"));
		m.appendKey(new UnitMenu("cu"));
		m.appendKey(new UnitMenu("ozfl"));
		m.appendKey(new UnitMenu("ozuk"));
		m.appendKey(new UnitMenu("tbsp"));
		m.appendKey(new UnitMenu("tsp"));
		m.appendKey(new UnitMenu("bbl"));
		m.appendKey(new UnitMenu("bu"));
		m.appendKey(new UnitMenu("pk"));
		m.appendKey(new UnitMenu("fbm"));
		return m;
	}

	private TeclaVirtual timeMenu() {
		Menu m = new Menu("TIME");
		m.appendKey(new UnitMenu("yr"));
		m.appendKey(new UnitMenu("d"));
		m.appendKey(new UnitMenu("h"));
		m.appendKey(new UnitMenu("min"));
		m.appendKey(new UnitMenu("s"));
		m.appendKey(new UnitMenu("Hz"));
		return m;
	}

	private TeclaVirtual speedMenu() {
		Menu m = new Menu("SPEED");
		m.appendKey(new UnitMenu("m/s"));
		m.appendKey(new UnitMenu("cm/s"));
		m.appendKey(new UnitMenu("ft/s"));
		m.appendKey(new UnitMenu("kph"));
		m.appendKey(new UnitMenu("mph"));
		m.appendKey(new UnitMenu("knot"));
		m.appendKey(new UnitMenu("c"));
		m.appendKey(new UnitMenu("ga"));
		return m;
	}

	private TeclaVirtual massMenu() {
		Menu m = new Menu("MASS");
		m.appendKey(new UnitMenu("kg"));
		m.appendKey(new UnitMenu("g"));
		m.appendKey(new UnitMenu("lb"));
		m.appendKey(new UnitMenu("oz"));
		m.appendKey(new UnitMenu("slug"));
		m.appendKey(new UnitMenu("lbt"));
		m.appendKey(new UnitMenu("ton"));
		m.appendKey(new UnitMenu("tonu"));
		m.appendKey(new UnitMenu("t"));
		m.appendKey(new UnitMenu("ozt"));
		m.appendKey(new UnitMenu("ct"));
		m.appendKey(new UnitMenu("grain"));
		m.appendKey(new UnitMenu("u"));
		m.appendKey(new UnitMenu("mol"));
		return m;
	}

	private TeclaVirtual forceMenu() {
		Menu m = new Menu("FORCE");
		m.appendKey(new UnitMenu("N"));
		m.appendKey(new UnitMenu("dyn"));
		m.appendKey(new UnitMenu("gf"));
		m.appendKey(new UnitMenu("kip"));
		m.appendKey(new UnitMenu("lbf"));
		m.appendKey(new UnitMenu("pdl"));
		return m;
	}

	private TeclaVirtual enrgMenu() {
		Menu m = new Menu("ENRG");
		m.appendKey(new UnitMenu("J"));
		m.appendKey(new UnitMenu("erg"));
		m.appendKey(new UnitMenu("kcal"));
		m.appendKey(new UnitMenu("cal"));
		m.appendKey(new UnitMenu("btu"));
		m.appendKey(new UnitMenu("ft*lb"));
		m.appendKey(new UnitMenu("ther"));
		m.appendKey(new UnitMenu("mev"));
		m.appendKey(new UnitMenu("ev"));
		return m;
	}

	private TeclaVirtual powrMenu() {
		Menu m = new Menu("POWR");
		m.appendKey(new UnitMenu("W"));
		m.appendKey(new UnitMenu("hp"));
		return m;
	}

	private TeclaVirtual pressMenu() {
		Menu m = new Menu("PRESS");
		m.appendKey(new UnitMenu("Pa"));
		m.appendKey(new UnitMenu("atm"));
		m.appendKey(new UnitMenu("bar"));
		m.appendKey(new UnitMenu("psi"));
		m.appendKey(new UnitMenu("torr"));
		m.appendKey(new UnitMenu("mmHg"));
		m.appendKey(new UnitMenu("inhg"));
		m.appendKey(new UnitMenu("inH20"));
		return m;
	}

	private TeclaVirtual tempMenu() {
		Menu m = new Menu("TEMP");
		m.appendKey(new UnitMenu("�C"));
		m.appendKey(new UnitMenu("�F"));
		m.appendKey(new UnitMenu("K"));
		m.appendKey(new UnitMenu("�R"));
		return m;
	}

	private TeclaVirtual elecMenu() {
		Menu m = new Menu("ELEC");
		m.appendKey(new UnitMenu("V"));
		m.appendKey(new UnitMenu("A"));
		m.appendKey(new UnitMenu("C"));
		m.appendKey(new UnitMenu("Ohm"));
		m.appendKey(new UnitMenu("F"));
		m.appendKey(new UnitMenu("W"));
		m.appendKey(new UnitMenu("Fdy"));
		m.appendKey(new UnitMenu("H"));
		m.appendKey(new UnitMenu("mho"));
		m.appendKey(new UnitMenu("S"));
		m.appendKey(new UnitMenu("T"));
		m.appendKey(new UnitMenu("Wb"));
		return m;
	}

	private TeclaVirtual anglMenu() {
		Menu m = new Menu("ANGL");
		m.appendKey(new UnitMenu("�"));
		m.appendKey(new UnitMenu("r"));
		m.appendKey(new UnitMenu("grad"));
		m.appendKey(new UnitMenu("arcmin"));
		m.appendKey(new UnitMenu("arcs"));
		m.appendKey(new UnitMenu("sr"));
		return m;
	}

	private TeclaVirtual lightMenu() {
		Menu m = new Menu("LIGHT");
		m.appendKey(new UnitMenu("fc"));
		m.appendKey(new UnitMenu("flam"));
		m.appendKey(new UnitMenu("lx"));
		m.appendKey(new UnitMenu("ph"));
		m.appendKey(new UnitMenu("sb"));
		m.appendKey(new UnitMenu("lm"));
		m.appendKey(new UnitMenu("cd"));
		m.appendKey(new UnitMenu("lam"));
		return m;
	}

	private TeclaVirtual radMenu() {
		Menu m = new Menu("RAD");
		m.appendKey(new UnitMenu("Gy"));
		m.appendKey(new UnitMenu("rad"));
		m.appendKey(new UnitMenu("rem"));
		m.appendKey(new UnitMenu("Sv"));
		m.appendKey(new UnitMenu("Bq"));
		m.appendKey(new UnitMenu("Ci"));
		m.appendKey(new UnitMenu("R"));
		return m;
	}

	private TeclaVirtual viscMenu() {
		Menu m = new Menu("VISC");
		m.appendKey(new UnitMenu("P"));
		m.appendKey(new UnitMenu("St"));
		return m;
	}
}
