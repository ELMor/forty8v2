/*
 * Created on 22-jul-2003
 *
 
 
 */
package f8.keyboard.hp48.menu;

import f8.CL;
import f8.keyboard.Menu;
import f8.keyboard.WithEnter;

/**
 * @author elinares
 * 
 * 
 * 
 */
public final class MODES extends Menu {
	public MODES() {
		super("MODES");
		appendKey(new WithEnter("STD", null, "DM", CL.DOU_MOD, CL.STD));
		appendKey(new WithEnter("FIX", null, "DM", CL.DOU_MOD, CL.FIX));
		appendKey(new WithEnter("SCI", null, "DM", CL.DOU_MOD, CL.SCI));
		appendKey(new WithEnter("ENG", null, "DM", CL.DOU_MOD, CL.ENG));

		appendKey(new WithEnter("SYM", "ChkSYM_", "SY", CL.SYM_MOD, 1));
		appendKey(new WithEnter("BEEP", "ChkBEEP_", "BE", CL.BEEP, 1));

		appendKey(new WithEnter("STK", "ChkSTK_", "ST", CL.LAS_STA, 1));
		appendKey(new WithEnter("ARG", "ChkARG_", "AR", CL.LAS_ARG, 1));
		appendKey(new WithEnter("CMD", "ChkCMD_", "CM", CL.LAS_CMD, 1));
		appendKey(new WithEnter("CNCT", "ChkCNC_", "CN", CL.CNCT, 1));
		appendKey(new WithEnter("ML", "ChkML_", "ML", CL.MULTILI, 1));
		appendKey(new WithEnter("CLK", "ChkCLK_", "CK", CL.CLOCK, 1));

		appendKey(new WithEnter("DEG", null, "AM", CL.ANG_MOD, CL.DEG));
		appendKey(new WithEnter("RAD", null, "AM", CL.ANG_MOD, CL.RAD));
		appendKey(new WithEnter("GRAD", null, "AM", CL.ANG_MOD, CL.GRAD));

		appendKey(new WithEnter("XYZ", "3D_1", "3D", CL.MOD_3D, CL.XYZ));
		appendKey(new WithEnter("RAZ", "3D_2", "3D", CL.MOD_3D, CL.RAZ));
		appendKey(new WithEnter("RAA", "3D_3", "3D", CL.MOD_3D, CL.RAA));

		appendKey(new WithEnter("HEX", null, "IM", CL.INT_MOD, CL.HEX));
		appendKey(new WithEnter("DEC", null, "IM", CL.INT_MOD, CL.DEC));
		appendKey(new WithEnter("OCT", null, "IM", CL.INT_MOD, CL.OCT));
		appendKey(new WithEnter("BIN", null, "IM", CL.INT_MOD, CL.BIN));

		appendKey(new WithEnter("FM,", "0_0", "FM", CL.PER_MOD, 1));
	}
}
