/*
 * Created on 29-ago-2003
 *
 
 
 */
package f8.keyboard.hp48.groups;

import f8.keyboard.Menu;

/**
 * @author elinares
 * 
 * 
 * 
 */
public final class Keyboard {
	
	public static Menu alphUp(){ return new AlphUp(); }

	public static Menu normMiddle() { return new NormMiddle(); }

	public static Menu alphMiddle() { return new AlphMiddle(); }

	public static Menu leftMiddle() { return new LeftMiddle(); }

	public static Menu rightMiddle() { return new RightMiddle(); }

	public static Menu normDown() { return new NormDown(); }

	public static Menu leftDown() { return new LeftDown(); }

	public static Menu rightDown() { return new RightDown(); }
	

}
