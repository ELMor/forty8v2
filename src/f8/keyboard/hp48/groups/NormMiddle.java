/*
 * Created on 22-jul-2003
 *
 
 
 */
package f8.keyboard.hp48.groups;

import f8.commands.Operation;
import f8.commands.storage.MENU;
import f8.exceptions.F8Exception;
import f8.keyboard.ENTER;
import f8.keyboard.Menu;
import f8.keyboard.NXT;
import f8.keyboard.Tecla;
import f8.keyboard.VAR;
import f8.keyboard.WithEnter;
import f8.keyboard.hp48.menu.MTH;
import f8.keyboard.hp48.menu.PRG;

/**
 * @author elinares
 * 
 * 
 * 
 */
public final class NormMiddle extends Menu {
	public NormMiddle() {
		super(null);
		appendKey(new MTH());
		appendKey(new PRG());
		appendKey(new Tecla("CST", // CST
				new Operation() {
					public void exec() throws F8Exception {
						MENU.showCustomMenu();
					}
				}));

		appendKey(new VAR());
		appendKey(new WithEnter("\u0090", "")); // Interactive Stack
		appendKey(new NXT());
		appendKey(new Tecla("'")); // '
		appendKey(new WithEnter("STO")); // STO
		appendKey(new WithEnter("EVAL")); // EVAL
		appendKey(new Tecla("\u008E", "")); // Muestra PICT
		appendKey(new Tecla("\u008F", "")); // Edita level1
		appendKey(new Tecla("\u008D", "SWAP")); // ->
		appendKey(new WithEnter("SIN")); // SIN
		appendKey(new WithEnter("COS")); // COS
		appendKey(new WithEnter("TAN")); // TAN
		appendKey(new WithEnter("\u0083")); // SQRT
		appendKey(new WithEnter("^")); // yEx
		appendKey(new WithEnter("INV"));
		appendKey(new ENTER());
		appendKey(new WithEnter("CHS"));
		appendKey(new Tecla("EEX", "E"));
		appendKey(new Tecla("DEL"));
		appendKey(new WithEnter("DROP"));
	}
}
