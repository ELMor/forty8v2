/*
 * Created on 22-jul-2003
 *
 
 
 */
package f8.keyboard.hp48.groups;

import f8.Ref;
import f8.commands.Operation;
import f8.exceptions.F8Exception;
import f8.keyboard.Menu;
import f8.keyboard.ShiftKey;
import f8.keyboard.Tecla;
import f8.keyboard.WithEnter;
import f8.keyboard.hp48.menu.PLOT;
import f8.keyboard.hp48.menu.SOLVE;
import f8.keyboard.hp48.menu.STAT;
import f8.keyboard.hp48.menu.SYMB;
import f8.keyboard.hp48.menu.TIME;
import f8.keyboard.hp48.menu.UNITS;

/**
 * @author elinares
 * 
 * 
 * 
 */
public final class LeftDown extends Menu {
	public LeftDown() {
		super(null);
		appendKey(new Tecla("USR"));
		appendKey(new Tecla("SOLVE", new Operation() {
			/*
			 * (non-Javadoc)
			 * 
			 * @see f8.kernel.Operation#exec()
			 */
			public void exec() {
				try {
					Ref.ref.setMenu(0, new SOLVE(), null, null);
				} catch (F8Exception e) {
				}
			}
		}));
		appendKey(new PLOT());
		appendKey(new SYMB());
		appendKey(new Tecla("()", 1));
		appendKey(new ShiftKey(1));
		appendKey(new TIME());
		appendKey(new STAT());
		appendKey(new UNITS());
		appendKey(new Tecla("[]", 1));
		appendKey(new ShiftKey(2));
		appendKey(new Tecla("RAD"));
		appendKey(new Tecla("STACK"));
		appendKey(new Tecla("CMD"));
		appendKey(new Tecla("\u00AB\u00BB", 1)); // Open-Close Prog <<>>
		appendKey(new Tecla("CONT"));
		appendKey(new WithEnter("="));
		appendKey(new Tecla(","));
		appendKey(new Tecla("\u0087", "Pi"));
		appendKey(new Tecla("{}", 1));
	}
}
