/*
 * Created on 22-jul-2003
 *
 
 
 */
package f8.keyboard.hp48.groups;

import f8.keyboard.Menu;
import f8.keyboard.Tecla;

/**
 * @author elinares
 * 
 * 
 * 
 */
public final class AlphMiddle extends Menu {
	public AlphMiddle() {
		super(null);
		appendKey(new Tecla("G"));
		appendKey(new Tecla("H"));
		appendKey(new Tecla("I"));
		appendKey(new Tecla("J"));
		appendKey(new Tecla("K"));
		appendKey(new Tecla("L"));
		appendKey(new Tecla("M"));
		appendKey(new Tecla("N"));
		appendKey(new Tecla("O"));
		appendKey(new Tecla("P"));
		appendKey(new Tecla("Q"));
		appendKey(new Tecla("R"));
		appendKey(new Tecla("S"));
		appendKey(new Tecla("T"));
		appendKey(new Tecla("U"));
		appendKey(new Tecla("V"));
		appendKey(new Tecla("W"));
		appendKey(new Tecla("X"));
		appendKey(new Tecla(""));
		appendKey(new Tecla("Y"));
		appendKey(new Tecla("Z"));
		appendKey(new Tecla(""));
		appendKey(new Tecla(""));
	}
}
