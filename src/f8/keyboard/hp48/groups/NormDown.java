/*
 * Created on 22-jul-2003
 *
 
 
 */
package f8.keyboard.hp48.groups;

import f8.keyboard.Alpha;
import f8.keyboard.Menu;
import f8.keyboard.ShiftKey;
import f8.keyboard.Tecla;
import f8.keyboard.WithEnter;

/**
 * @author elinares
 * 
 * 
 * 
 */
public final class NormDown extends Menu {
	public NormDown() {
		super("");
		appendKey(new Alpha());
		appendKey(new Tecla("7"));
		appendKey(new Tecla("8"));
		appendKey(new Tecla("9"));
		appendKey(new WithEnter("/"));
		appendKey(new ShiftKey(1));
		appendKey(new Tecla("4"));
		appendKey(new Tecla("5"));
		appendKey(new Tecla("6"));
		appendKey(new WithEnter("*"));
		appendKey(new ShiftKey(2));
		appendKey(new Tecla("1"));
		appendKey(new Tecla("2"));
		appendKey(new Tecla("3"));
		appendKey(new WithEnter("-"));
		appendKey(new Tecla("ON"));
		appendKey(new Tecla("0"));
		appendKey(new Tecla("."));
		appendKey(new Tecla("SPC", " "));
		appendKey(new WithEnter("+"));
	}
}
