/*
 * Created on 22-jul-2003
 *
 
 
 */
package f8.keyboard;

import f8.CalcGUI;

/**
 * @author elinares
 * 
 * 
 * 
 */
public final class PREV extends Tecla {
	public PREV() {
		super("PREV");
	}

	public void pressed(CalcGUI cg) {
		CF cf = cg.getCalcFisica();
		cf.prevUpMenuPage();
		cg.switchButtonGroup(1);
	}
}
