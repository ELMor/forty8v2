/*
 * Created on 24-sep-2003
 *
 
 
 */
package f8.exceptions;

import f8.commands.Command;
import f8.keyboard.TeclaVirtual;

;

/**
 * @author elinares
 * 
 * 
 * 
 */
public class BadArgumentValueException extends f8.exceptions.F8Exception {
	/**
	 * 
	 */
	public BadArgumentValueException(Command c) {
		super(c);
	}

	/**
	 * @param message
	 */
	public BadArgumentValueException(String message) {
		super(message);
	}

	/**
	 * @param v
	 */
	public BadArgumentValueException(TeclaVirtual v) {
		super(v);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.rtExceptions.F8Exception#getNdx()
	 */
	public int getNdx() {

		return ICalcErr.BadArgumentValue;
	}

}
