/*
 * Created on 24-sep-2003
 *
 
 
 */
package f8.exceptions;

import f8.commands.Command;
import f8.keyboard.TeclaVirtual;

;

/**
 * @author elinares
 * 
 * 
 * 
 */
public class BadArgumentTypeException extends f8.exceptions.F8Exception {
	/**
	 * 
	 */
	public BadArgumentTypeException(Command c) {
		super(c);
	}

	/**
	 * @param message
	 */
	public BadArgumentTypeException(String message) {
		super(message);
	}

	/**
	 * @param v
	 */
	public BadArgumentTypeException(TeclaVirtual v) {
		super(v);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.rtExceptions.F8Exception#getNdx()
	 */
	public int getNdx() {

		return ICalcErr.BadArgumentType;
	}

}
