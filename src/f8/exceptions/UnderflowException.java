/*
 * Created on 24-sep-2003
 *
 
 
 */
package f8.exceptions;

import f8.commands.Command;

;

/**
 * @author elinares
 * 
 * 
 * 
 */
public class UnderflowException extends F8Exception {

	/**
	 * 
	 */
	public UnderflowException(Command c) {
		super(c);

	}

	/**
	 * @param message
	 */
	public UnderflowException(String message) {
		super(message);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.rtExceptions.F8Exception#getNdx()
	 */
	public int getNdx() {
		return ICalcErr.Underflow;
	}

}
