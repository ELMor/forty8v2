/*
 * Created on 24-sep-2003
 *
 
 
 */
package f8.exceptions;

import f8.commands.Command;

;

/**
 * @author elinares
 * 
 * 
 * 
 */
public class ParseException extends f8.exceptions.F8Exception {
	/**
	 * 
	 */
	public ParseException(Command c) {
		super(c);
	}

	/**
	 * @param message
	 */
	public ParseException(String message) {
		super(message);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.rtExceptions.F8Exception#getNdx()
	 */
	public int getNdx() {

		return ICalcErr.BadArgumentType;
	}

}
