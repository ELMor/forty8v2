package f8.objects.build;

import f8.CL;
import f8.DataStream;
import f8.Ref;
import f8.Storable;
import f8.commands.NonAlgebraic;
import f8.exceptions.F8Exception;
import f8.exceptions.TooFewArgumentsException;
import f8.objects.Stackable;
import f8.objects.types.Double;
import f8.objects.types.Unit;

public final class TO_UNIT extends NonAlgebraic {
	public TO_UNIT() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 508;
	}

	public Storable getInstance() {
		return new TO_UNIT();
	}

	public void loadState(DataStream ds) {
	}

	public void saveState(DataStream ds) {
	}

	public String toString() {
		return ("->UNIT");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.StkOb#exec()
	 */
	public void exec() throws F8Exception {
		CL cl = Ref.ref.getCalcLogica();
		Stackable[] arg = new Stackable[2];
		arg[0] = new Double(0);
		arg[1] = new Unit(null);
		String dsp = cl.dispatch(arg);
		if (dsp != null) {
			cl.push(new Unit(((Double) arg[0]).x, ((Unit) arg[1]).getUnidad()));
		} else {
			throw new TooFewArgumentsException(this);
		}
	}
}
