/*
 * Created on 14-sep-2003
 *
 
 
 */
package f8.objects.build;

import f8.CL;
import f8.DataStream;
import f8.Ref;
import f8.Storable;
import f8.UtilFactory;
import f8.Vector;
import f8.commands.NonAlgebraic;
import f8.exceptions.BadArgumentTypeException;
import f8.exceptions.F8Exception;
import f8.exceptions.TooFewArgumentsException;
import f8.objects.types.Double;
import f8.objects.types.Lista;

/**
 * @author elinares
 * 
 * 
 * 
 */
public class TO_LIST extends NonAlgebraic {
	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.io.Storable#getID()
	 */
	public int getID() {
		return 302;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.io.Storable#getInstance()
	 */
	public Storable getInstance() {
		return this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.io.Storable#loadState(f8.platform.io.DataStream)
	 */
	public void loadState(DataStream ds) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.io.Storable#saveState(f8.platform.io.DataStream)
	 */
	public void saveState(DataStream ds) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.StkOb#exec()
	 */
	public void exec() throws F8Exception {
		CL cl = Ref.ref.getCalcLogica();
		if (cl.check(1)) {
			try {
				Double lon = (Double) cl.pop();
				int len = lon.intValue();
				if (cl.check(len)) {
					Vector v = UtilFactory.newVector();
					for (int i = len - 1; i >= 0; i--)
						v.add(cl.peek(i));
					cl.pop(len);
					cl.push(new Lista(v));
				} else {
					cl.push(lon);
					throw new TooFewArgumentsException(this);
				}
			} catch (Exception k) {
				throw new BadArgumentTypeException(this);
			}
		} else {
			throw new TooFewArgumentsException(this);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return "->LIST";
	}
}
