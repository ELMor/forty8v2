/*
 * Created on 11-ago-2003
 *
 
 
 */
package f8.objects;


/**
 * @author elinares
 * 
 * 
 * 
 */
public final class StringReader implements Reader {
	String text;

	int pos = 0;

	public StringReader(String t) {
		text = t;
	}

	public char read() {
		if (pos >= text.length()) {
			return (char) -1;
		}
		return text.charAt(pos++);
	}
}
