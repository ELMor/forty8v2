/*
 * Created on 13-ago-2003
 *
 
 
 */
package f8.objects.types;

import f8.DataStream;
import f8.Storable;
import f8.UtilFactory;
import f8.Vector;
import f8.commands.alg.CommandSequence;
import f8.exceptions.IndexRangeException;
import f8.objects.Stackable;

/**
 * @author elinares
 * 
 * 
 * 
 */
public final class Lista extends CommandSequence {
	/**
	 * Representa una lista
	 * 
	 * @param a
	 */
	public Lista(Vector a) {
		super(a);
	}

	public String getTypeName() {
		return "Lista";
	}

	public int getID() {
		return 120;
	}

	public String toString() {
		String ret = getTag();
		if (!ret.equals("")) {
			ret += ":";
		}
		return ret + "{ " + super.toString() + " }";
	}

	public Stackable copia() {
		Vector copia = UtilFactory.newVector();
		for (int i = 0; i < obList.size(); i++) {
			if (obList.elementAt(i) instanceof Stackable) {
				copia.add(((Stackable) obList.elementAt(i)).copia());
			} else {
				copia.add(obList.elementAt(i));
			}
		}
		return new Lista(copia);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.Storable#loadState(f8.platform.DataStream)
	 */
	public void loadState(DataStream ds) {
		super.loadState(ds);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.Storable#saveState(f8.platform.DataStream)
	 */
	public void saveState(DataStream ds) {
		super.saveState(ds);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.Storable#getInstance()
	 */
	public Storable getInstance() {
		return this;
	}

	public Stackable add(Stackable esto) throws IndexRangeException {
		if (esto instanceof Lista) {
			Lista l = (Lista) esto;
			for (int i = 0; i < l.size(); i++)
				obList.add(l.get(i));
		} else {
			obList.add(esto);
		}
		return this;
	}

}
