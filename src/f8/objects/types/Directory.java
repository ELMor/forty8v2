/*
 * Created on 06-ago-2003
 *
 
 
 */
package f8.objects.types;

import f8.CL;
import f8.DataStream;
import f8.Hashtable;
import f8.Ref;
import f8.Storable;
import f8.UtilFactory;
import f8.Vector;
import f8.commands.Command;
import f8.keyboard.CF;
import f8.keyboard.VAR;
import f8.objects.Stackable;

/**
 * @author elinares
 * 
 * 
 * 
 */
public final class Directory extends Stackable {
	Hashtable ht = UtilFactory.newHashtable();

	String nombre;

	public Directory(Directory parent, String name) {
		if (name == null) {
			return;
		}
		if (name.startsWith("'")) {
			name = name.substring(1);
		}

		if (name.endsWith("'")) {
			name = name.substring(0, name.length() - 1);
		}

		nombre = name;

		if (!nombre.equals("System") && !nombre.equals("")) {
			parent.put(nombre, this);
		}
	}

	public Stackable copia() {
		return null; // Los directorios no los quiero copiar
	}

	public String getTypeName() {
		return "Directory";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.io.Storable#getID()
	 */
	public int getID() {
		return 127;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.io.Storable#getInstance()
	 */
	public Storable getInstance() {
		return new Directory(null, "");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.io.Storable#saveState(f8.platform.io.DataStream)
	 */
	public void saveState(DataStream ds) {
		super.saveState(ds);
		ds.writeStringSafe(nombre);

		Vector k = ht.getKeys();
		ds.writeInt(k.size());

		for (int i = 0; i < k.size(); i++) {
			Storable item = (Storable) ht.get(k.elementAt(i));
			ds.writeStringSafe((String) k.elementAt(i));
			ds.writeInt(item.getID());
			item.saveState(ds);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.io.Storable#loadState(f8.platform.io.DataStream)
	 */
	public void loadState(DataStream ds) {
		super.loadState(ds);
		nombre = ds.readStringSafe();

		int sz = ds.readInt();

		for (int i = 0; i < sz; i++) {
			String n = ds.readStringSafe();
			Storable obj = Command.loadFromStorage(ds);
			ht.put(n, obj);
		}
	}

	public Hashtable getHT() {
		return ht;
	}

	public String getNombre() {
		return nombre;
	}

	public void put(String name, Object obj) {
		ht.put(name, obj);
	}

	public Command get(String name) {
		return (Command) ht.get(name);
	}

	public void exec() {
		Ref.ref.getCalcLogica().changeToDir(nombre);

		CF cf = Ref.ref.getCalcFisica();

		if (cf.getUpMenu() instanceof VAR) {
			Ref.ref.setMenu(1, new VAR(), null, null);
		}
	}

	public String toString() {
		String ret = "DIR ";
		Vector v = ht.getKeys();
		for (int i = 0; i < v.size(); i++) {
			ret += (" " + (String) v.elementAt(i) + " " + ((Command) ht.get(v
					.elementAt(i))).toString());
		}
		return ret + " END";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.StkObType#decompose(f8.kernel.CL)
	 */
	public boolean decompose(CL cl) {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Stackable#tolerance(java.lang.Object, double)
	 */
	public boolean tolerance(Object obj, double tol) {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Stackable#sign()
	 */
	public Stackable sign() {
		return new Double(0);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Stackable#zero()
	 */
	public Stackable zero() {
		return new Directory(null, null);
	}

}
