/*
 * Created on 08-oct-2003
 *
 
 
 */
package f8.apps.display;

import f8.CL;
import f8.CalcGUI;
import f8.DataStream;
import f8.Ref;
import f8.Storable;
import f8.apps.LCDControl;
import f8.apps.solver.EqSolv;
import f8.commands.Command;
import f8.commands.alg.CommandSequence;
import f8.commands.alg.Ejecutable;
import f8.exceptions.BadArgumentValueException;
import f8.exceptions.F8Exception;
import f8.objects.Stackable;

/**
 * @author elinares
 * 
 * 
 * 
 */
public class DRAW extends Command {
	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.Storable#getID()
	 */
	public int getID() {
		return 6000;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.Storable#getInstance()
	 */
	public Storable getInstance() {
		return this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.Storable#loadState(f8.platform.DataStream)
	 */
	public void loadState(DataStream ds) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.Storable#saveState(f8.platform.DataStream)
	 */
	public void saveState(DataStream ds) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Command#isInfix()
	 */
	public boolean isInfix() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Command#isOperator()
	 */
	public boolean isOperator() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Operation#exec()
	 */
	public void exec() throws F8Exception {
		try {
			CalcGUI cg = Ref.ref;
			CL cl = cg.getCalcLogica();
			LCDControl lcdc = cg.getLCD();
			// Mostramos el LCD
			cg.selectUpperTab(1);
			PPAR.initPPAR();
			Stackable eq = (Stackable) cl.lookupUser("EQ", false, true, false);
			CommandSequence run = ((Ejecutable) eq).getEjecutable();

			CSys usrcs = CSys.getCoorSystem("usr");
			CSys lcdcs = CSys.getCoorSystem("lcd");
			lcdc.reset(PPAR.width, PPAR.height, lcdcs.width() / 2, lcdcs
					.height() / 2);
			lcdc.showAxes();
			// Inicializacion
			int endX = (int) lcdcs.right().v[0];
			double initY = EqSolv.fedxDouble(run, PPAR.indep, PPAR.Xmin);
			CSys.Point init = usrcs.coor(PPAR.Xmin, initY);
			lcdc.initPoint(init);
			CSys.Vector step = lcdcs.vector(2, 0);
			for (int i = 0; i < endX; i += 2) {
				init = usrcs.transform(init.plus(step));
				init.v[1] = EqSolv.fedxDouble(run, PPAR.indep, init.v[0]);
				lcdc.nextPoint(init);
			}
		} catch (ClassCastException e) {
			throw new BadArgumentValueException("PPAR is bad, remove");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return "DRAW";
	}
}
