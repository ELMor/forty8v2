package f8.apps.display;

import f8.CL;
import f8.DataStream;
import f8.Ref;
import f8.Storable;
import f8.apps.LCDControl;
import f8.commands.NonAlgebraic;
import f8.exceptions.BadArgumentTypeException;
import f8.exceptions.F8Exception;
import f8.exceptions.TooFewArgumentsException;


public final class MARK extends NonAlgebraic {
	public MARK() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 401;
	}

	public Storable getInstance() {
		return new MARK();
	}

	public void loadState(DataStream ds) {
	}

	public void saveState(DataStream ds) {
	}

	public void exec() throws F8Exception {
		LCDControl lcd = Ref.ref.getLCD();
		CL cl = Ref.ref.getCalcLogica();
		if (cl.check(1)) {
			CSys.Point c = StackChecker.UserCoordinates(cl.peek());
			if (c != null) {
				cl.pop();
				lcd.setMark(c);
			} else {
				throw new BadArgumentTypeException(this);
			}
		} else {
			throw new TooFewArgumentsException(this);
		}
	}

	public String toString() {
		return ("MARK");
	}
}
