/*
 * Created on 09-oct-2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package f8.apps.display;

import f8.CL;
import f8.Ref;
import f8.UtilFactory;
import f8.Vector;
import f8.commands.storage.STO;
import f8.exceptions.CircularReferenceException;
import f8.exceptions.IndexRangeException;
import f8.objects.types.Complex;
import f8.objects.types.Double;
import f8.objects.types.Lista;
import f8.objects.types.Literal;

/**
 * @author elinares
 * 
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class PPAR {
	public static double Xmin;

	public static double Xmax;

	public static double Ymin;

	public static double Ymax;

	public static double Ox;

	public static double Oy;

	public static String indep;

	public static String depen;

	public static double height;

	public static double width;

	public static void savePPAR() {
		Lista ppar;
		Vector val = UtilFactory.newVector();
		val.add(new Complex(Xmin, Ymin));
		val.add(new Complex(Xmax, Ymax));
		val.add(new Literal(indep));
		val.add(new Double(0));
		val.add(new Complex(0, 0));
		val.add(new Literal("FUNCTION"));
		val.add(new Literal(depen));
		ppar = new Lista(val);
		try {
			STO.storeVar("PPAR", ppar);
		} catch (CircularReferenceException e) {
		}
	}

	public static Lista initPPAR() {
		/* Vamos a crear PPAR si no lo esta ya */
		Lista ppar = loadPPAR();
		if (ppar == null) {
			Vector val = UtilFactory.newVector();
			val.add(new Complex(-6.5, -3.2));
			val.add(new Complex(6.5, 3.2));
			val.add(new Literal("X"));
			val.add(new Double(0));
			val.add(new Complex(0, 0));
			val.add(new Literal("FUNCTION"));
			val.add(new Literal("Y"));
			ppar = new Lista(val);
			try {
				STO.storeVar("PPAR", ppar);
			} catch (CircularReferenceException e) {
			}
		}
		try {
			Xmin = ((Complex) ppar.get(0)).re;
			Ymin = ((Complex) ppar.get(0)).im;
			Xmax = ((Complex) ppar.get(1)).re;
			Ymax = ((Complex) ppar.get(1)).im;
			Ox = ((Complex) ppar.get(4)).re;
			Oy = ((Complex) ppar.get(4)).im;
			indep = ((Literal) ppar.get(2)).nombre;
			depen = ((Literal) ppar.get(6)).nombre;
			height = Ymax - Ymin;
			width = Xmax - Xmin;
		} catch (IndexRangeException e) {
		}
		return ppar;
	}

	public static Lista loadPPAR() {
		try {
			/* Vamos a crear PPAR si no lo esta ya */
			CL cl = Ref.ref.getCalcLogica();
			return (Lista) cl.lookupUser("PPAR", false, true, false);
		} catch (ClassCastException e) {
			return null;
		}
	}
}
