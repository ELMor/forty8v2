package f8;

import antlr.collections.AST;
import f8.commands.Command;
import f8.commands.prog.debug.Debuggable;
import f8.commands.stk.DUP;
import f8.commands.units.UnitManager;
import f8.exceptions.F8Exception;
import f8.exceptions.ICalcErr;
import f8.objects.Stackable;
import f8.objects.types.Directory;
import f8.objects.types.InfixExp;
import f8.objects.types.Literal;
import f8.objects.types.Unit;

public final class CLI2 implements CL {
	// pila de diccionarios de sistema y usuario
	private Stack dirStack;

	// Diccionario de usuario
	public Directory homeDir;

	// Diccionario de sistema
	public Directory systemDir;

	private Stack lastStack = null;

	// Pila de diccionarios de variables locales
	private Stack localVarStack;

	// operator stack: RPN operators and data to be processed
	private Stack userStack;

	public Stack debugStack = null;

	private boolean isCatalogOnDevice = false;

	// Modos de representacion
	private int[] settings = new int[] { 0, CL.DEG, 1, 1, 1, CL.STD, 0, CL.DEC,
			1, 1, 1, 1, CL.XYZ, CL.COMMA, 0, 1 };

	public CLI2(boolean existFile, DataStream ds) {
		Ref.ref.setCalcLogica(this);
		userStack = UtilFactory.newStack();
		dirStack = UtilFactory.newStack();
		localVarStack = UtilFactory.newStack();
		debugStack = UtilFactory.newStack();
		isCatalogOnDevice = existFile;
        systemDir           =new Directory(null, "System");
		homeDir = new Directory(systemDir, "HOME");
		new CommandDict(); //Para cargar el diccionario de comandos estaticos
		dirStack.push(systemDir);
		dirStack.push(homeDir);

		if (existFile) {
			Ref.ref.setInitialEdit(loadFromStorage(ds));
		}
	}

	public void changeToDir(String dname) {
		Directory dir = null;

		try {
			dir = (Directory) currentDict().getHT().get(dname);
		} catch (Exception e) {
			dir = null;
		}

		if (dir != null) {
			dirStack.push(dir);
			Ref.ref.refreshStatusLabels();
		}
	}

	// - operator stack -------------------------------
	public boolean check(int n) {
		if (userStack.size() < n) {
			return (false);
		} else {
			return (true);
		}
	}

	public Directory currentDict() {
		int N = dirStack.size();

		return ((Directory) dirStack.elementAt(N - 1));
	}

	public Hashtable dictPeek(int n) {
		int N = dirStack.size();

		return ((Directory) dirStack.elementAt(N - n - 1)).getHT();
	}

	public int dictStackSize() {
		return (dirStack.size());
	}

	public String dispatch(Stackable[] arg) {
		String ret = "";

		if (userStack.size() < arg.length) {
			return null;
		}

		for (int i = arg.length - 1; i >= 0; i--) {
			String tn = null;
			if (arg[i] != null) {
				tn = arg[i].getTypeName();
			}
			arg[i] = (Stackable) pop();
			String tnr = arg[i].getTypeName();
			if ((tn != null) && !(tn.equals(tnr))) {
				error(ICalcErr.BadArgumentType);
				return null;
			}
			ret += arg[i].getTypeName();
		}

		return ret;
	}

	public void enter(String input) throws F8Exception {
		if (input.equals("")) {
			new DUP().exec();
			return;
		}

		Vector vItems = UtilFactory.newVector();
		Stackable.parse(input, vItems);

		while (vItems.size() > 0) {
			AST nxtObj = (AST) vItems.get(0);
			if (nxtObj != null) {
				Command it = Command.createFromAST(nxtObj);
				if (!(it instanceof Stackable) || (it instanceof Literal)) {
					it.exec();
				} else {
					it.store();
				}
			}
			vItems.removeElementAt(0);
		}
	}

	// ---------------------------------------------
	public void error(Command it, int n) {
		error(ICalcErr.Name[n] + "\n with <" + it.toString() + ">");
	}

	public void error(F8Exception f8e) {
		String com = f8e.getPartic();
		error(com + " Error: |" + f8e.toString());
	}

	public void error(int i) {
		output(ICalcErr.Name[i]);
	}

	public void error(String msg) {
		output(msg);
	}

	public String getCurrentDirName() {
		String dir = "{";

		for (int i = 1; i < dirStack.size(); i++) {
			dir += (" " + ((Directory) dirStack.elementAt(i)).getNombre());
		}

		return dir + " }";
	}

	public Hashtable getCurrentLocalEnvir() {
		if (0 == localEnvirCount()) {
			return null;
		}

		return (Hashtable) localVarStack.peek();
	}

	/**
	 * @return
	 */
	public Debuggable getProcDebug() {
		return (Debuggable) debugStack.pop();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.CL#getSetting(int)
	 */
	public int getSetting(int setting) {
		return settings[setting];
	}

	public Hashtable getUserDict() {
		return homeDir.getHT();
	}

	public void homeDir() {
		while (!currentDict().getNombre().equals("HOME")) {
			dirStack.pop();
		}
	}

	public void install(Command it) {
		currentDict().put(it.toString(), it);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.CL#isSetting(int)
	 */
	public boolean isSetting(int setting) {
		return (settings[setting] == 1) ? true : false;
	}

	public String loadFromStorage(DataStream ds) {
		ds.setRecordPos(1);
		// Cargamos los modos
		for (int i = 0; i < settings.length; i++) {
			settings[i] = ds.readInt();
		}

		// Cargamos el userDict
		int sz = ds.readInt();

		for (int i = 0; i < sz; i++) {
			String key = ds.readStringSafe();
			Storable val = Command.loadFromStorage(ds);
			homeDir.put(key, val);
		}

		// Y el Stack
		int szOs = ds.readInt();

		for (int posOs = 0; posOs < szOs; posOs++) {
			userStack.push(Command.loadFromStorage(ds));
		}

		// Finalmente se retorna la linea de edicion
		String edit = ds.readStringSafe();
		ds.close();

		return edit;
	}

	public int localEnvirCount() {
		return localVarStack.size();
	}

	public Command lookup(String name) {
		int n = localVarStack.size();
		Command x = null;

		// Primero buscamos en la pila de diccionarios locales
		while ((x == null) && (n > 0)) {
			Hashtable h = ((Hashtable) localVarStack.elementAt(--n));
			x = (Stackable) h.get(name);
			if(x!=null)
				return x;
		}
		
		// luego en la de usuarios y en la de sistema
		n = dictStackSize();
		while (n > 0) {
			Hashtable h = ((Directory) dirStack.elementAt(--n)).getHT();
			x = (Command) h.get(name);
			if(x!=null)
				return x;
		}
		
		//Finalmente en el diccionario de sistema
		return CommandDict.getInstance(name);
	}

	public Command lookupUser(String name, boolean user, boolean userDefault,
			boolean local) {
		int n;
		Command x = null;
		if (local) {
			n = localVarStack.size();
			x = null;
			// Primero buscamos en la pila de diccionarios locales
			while ((x == null) && (n > 0)) {
				Hashtable h = ((Hashtable) localVarStack.elementAt(--n));
				x = (Stackable) h.get(name);
			}
			if (x != null) {
				return x;
			}
		}

		n = dictStackSize();
		if (userDefault) {
			Hashtable h = ((Directory) dirStack.elementAt(--n)).getHT();
			x = (Command) h.get(name);
			if (x != null) {
				return x;
			}
		}
		if (user) {
			// luego en la de usuarios (no en la de sistema:n>1)
			while ((x == null) && (n > 1)) {
				Hashtable h = ((Directory) dirStack.elementAt(--n)).getHT();
				x = (Command) h.get(name);
			}
		}

		return x;
	}

	public void output(Command it) {
		output(it.toString());
	}

	public void output(String msg) {
		Ref.ref.output(msg);
		Ref.ref.refresh(false);
	}

	// ---------------------------------------------
	public void pause() {
		;
	}

	public void pause(int seconds) {
		seconds = seconds + 1;
	}

	public Stackable peek() {
		int n = userStack.size();
		Stackable it = (Stackable) userStack.elementAt(n - 1);

		return (it);
	}

	public Stackable peek(int i) {
		int n = userStack.size();
		Stackable it = (Stackable) userStack.elementAt(n - 1 - i);

		return (it);
	}

	public void poke(int i, Command it) {
		userStack.poke(i, it);
	}

	public Stackable pop() {
		Stackable ret = (Stackable) userStack.pop();

		if (ret == null) {
			error(ICalcErr.TooFewArguments);
		}

		return ret;
	}

	public void pop(int n) {
		userStack.pop(n);
	}

	public void popLocalEnvir() {
		localVarStack.pop();
	}

	public void push(Stackable it) {
		Stackable copy;
		if (it instanceof Literal) {
			copy = new InfixExp(it.getAST());
		} else {
			copy = it.copia();
		}
		copy.setTag(it.getTag());
		userStack.push(copy);
	}

	public void pushLocalEnvir() {
		Hashtable h = UtilFactory.newHashtable();
		localVarStack.push(h);
	}

	public void restoreStack() {
		if (lastStack != null) {
			userStack = lastStack;
			lastStack = null;
		}
	}

	public void saveStack() {
		lastStack = UtilFactory.newStack();

		for (int i = 0; i < userStack.size(); i++) {
			lastStack.push(userStack.elementAt(userStack.size() - 1 - i));
		}
	}

	public void saveToStorage(String edit, DataStream ds) {
		// Grabamos las unidades (si no lo est�n ya)
		ds.setRecordPos(0);
		ds.addRecord();

		// Ahora User y Stack
		// Grabamos los modos
		for (int i = 0; i < settings.length; i++) {
			ds.writeInt(settings[i]);
		}

		// Grabamos el diccionario de datos
		Vector keys = homeDir.getHT().getKeys();
		int sz = keys.size();
		ds.writeInt(sz);

		for (int i = 0; i < sz; i++) {
			String skey = (String) keys.elementAt(i);
			ds.writeStringSafe(skey);

			Command value = (Command) homeDir.get(skey);
			ds.writeInt(value.getID());
			value.saveState(ds);
		}

		// Ahora opStack
		int szOs = userStack.size();
		ds.writeInt(szOs);

		for (int posOs = 0; posOs < szOs; posOs++) {
			Command it = (Command) userStack.elementAt(posOs);
			ds.writeInt(it.getID());
			it.saveState(ds);
		}

		// Finalmente la linea de edicion
		ds.writeStringSafe(edit);
	}

	public void setOpStack(Stack ops) {
		userStack = ops;
	}

	/**
	 * @param proc
	 */
	public void setProcDebug(Debuggable proc) {
		debugStack.push(proc);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.CL#setSettings(int, boolean)
	 */
	public void setSettings(int setting, boolean val) {
		settings[setting] = val ? 1 : 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.CL#setSettings(int, int)
	 */
	public void setSettings(int setting, int val) {
		settings[setting] = val;
	}

	public int size() {
		return (userStack.size());
	}

	public String stackToString() {
		StringBuffer s = new StringBuffer();

		for (int i = 0; i < userStack.size(); i++) {
			Command it = (Command) userStack.elementAt(i);
			s.append(it.toString());
			s.append('\n');
		}

		return (s.toString());
	}

	public String toString() {
		String s = new String();

		for (int i = 0; i < userStack.size(); i++) {
			Command it = (Command) userStack.elementAt(i);
			s += ((userStack.size() - i) + ": " + it.toString() + "\n");
		}

		return (s);
	}

	public void upDir() {
		if (!currentDict().getNombre().equals("HOME")) {
			dirStack.pop();
		}
	}
}
