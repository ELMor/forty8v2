package f8.modes;

import f8.CL;
import f8.DataStream;
import f8.Ref;
import f8.Storable;

public final class ChkARG extends CheckGroup {
	public ChkARG() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 499;
	}

	public Storable getInstance() {
		return new ChkARG();
	}

	public void loadState(DataStream ds) {
	}

	public void saveState(DataStream ds) {
	}

	public boolean execBefore() {
		CL cl = Ref.ref.getCalcLogica();
		cl.setSettings(CL.LAS_ARG, !cl.isSetting(CL.LAS_ARG));
		return false;
	}

	public String toString() {
		return ("ChkARG_");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.mod.CheckGroup#getGroupName()
	 */
	public String getGroupName() {
		return "AR";
	}
}
