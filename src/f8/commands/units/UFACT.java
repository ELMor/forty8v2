package f8.commands.units;

import f8.CL;
import f8.DataStream;
import f8.Ref;
import f8.Storable;
import f8.commands.NonAlgebraic;
import f8.exceptions.F8Exception;
import f8.exceptions.TooFewArgumentsException;
import f8.objects.Stackable;
import f8.objects.types.Unit;

public final class UFACT extends NonAlgebraic {
	public UFACT() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 103;
	}

	public Storable getInstance() {
		return new UFACT();
	}

	public void loadState(DataStream ds) {
	}

	public void saveState(DataStream ds) {
	}

	public String toString() {
		return ("UFACT");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.StkOb#exec()
	 */
	public void exec() throws F8Exception {
		CL cl = Ref.ref.getCalcLogica();
		Stackable[] arg = new Stackable[2];
		arg[0] = new Unit(null);
		arg[1] = arg[0];
		String dsp = cl.dispatch(arg);
		if (dsp != null) {
			Unit from = (Unit) arg[0];
			Unit to = (Unit) arg[1];
			cl.push(from.ufact(cl, to));
		} else {
			throw new TooFewArgumentsException(this);
		}
	}

}
