package f8.commands.alg.matrx;

import f8.DataStream;
import f8.Storable;
import f8.commands.Dispatch2;
import f8.exceptions.F8Exception;
import f8.exceptions.IndexRangeException;
import f8.objects.Stackable;
import f8.objects.types.Int;
import f8.objects.types.Matrix;

public final class Gauss extends Dispatch2 {
	public Gauss() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 64;
	}

	public Storable getInstance() {
		return new Gauss();
	}

	public void loadState(DataStream ds) {
	}

	public void saveState(DataStream ds) {
	}

	public String toString() {
		return ("GAUSS");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch2#prfIntMatrix(f8.kernel.types.Int,
	 *      f8.kernel.types.Matrix)
	 */
	public Stackable prfIntMatrix(Int a, Matrix b) throws F8Exception {
		Matrix M = b;
		int C = a.intValue();

		if ((C >= 0) && (C <= M.columnNumber())) {
			MatrixOperations.reduce(M.x, M.rowNumber(), C);
			return M;
		} else {
			throw new IndexRangeException(this);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isInfix()
	 */
	public boolean isInfix() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isOperator()
	 */
	public boolean isOperator() {
		return false;
	}

}
