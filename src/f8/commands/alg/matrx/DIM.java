package f8.commands.alg.matrx;

import f8.DataStream;
import f8.Storable;
import f8.commands.Dispatch1;
import f8.exceptions.F8Exception;
import f8.objects.Stackable;
import f8.objects.types.Int;
import f8.objects.types.Matrix;

public final class DIM extends Dispatch1 {
	public DIM() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 63;
	}

	public Storable getInstance() {
		return new DIM();
	}

	public void loadState(DataStream ds) {
	}

	public void saveState(DataStream ds) {
	}

	public String toString() {
		return ("DIM");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch1#prfMatrix(f8.kernel.types.Matrix)
	 */
	public Stackable prfMatrix(Matrix a) throws F8Exception {
		return new Int(a.size());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isInfix()
	 */
	public boolean isInfix() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isOperator()
	 */
	public boolean isOperator() {
		return false;
	}

}
