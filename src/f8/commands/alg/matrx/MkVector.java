package f8.commands.alg.matrx;

import f8.DataStream;
import f8.Storable;
import f8.commands.Dispatch1;
import f8.exceptions.F8Exception;
import f8.exceptions.InvalidDimensionException;
import f8.objects.Stackable;
import f8.objects.types.Double;
import f8.objects.types.F8Vector;

public final class MkVector extends Dispatch1 {
	public MkVector() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 69;
	}

	public Storable getInstance() {
		return new MkVector();
	}

	public void loadState(DataStream ds) {
	}

	public void saveState(DataStream ds) {
	}

	public String toString() {
		return ("VECTOR");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch1#prfDouble(f8.kernel.types.Double)
	 */
	public Stackable prfDouble(Double x) throws F8Exception {
		int c = (int) x.doubleValue();

		if (c >= 0) {
			return (new F8Vector(c));
		} else {
			throw new InvalidDimensionException(this);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isInfix()
	 */
	public boolean isInfix() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isOperator()
	 */
	public boolean isOperator() {
		return false;
	}

}
