/*
 * Created on 03-oct-2003
 *
 
 
 */
package f8.commands.prog.debug;

import f8.CL;
import f8.CalcGUI;
import f8.Ref;
import f8.commands.Operation;
import f8.exceptions.BadArgumentTypeException;
import f8.exceptions.F8Exception;
import f8.exceptions.TooFewArgumentsException;

/**
 * @author elinares
 * 
 * 
 * 
 */
public class DBUG extends Operation {

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Operation#exec()
	 */
	public void exec() throws F8Exception {
		CalcGUI cg = Ref.ref;
		CL cl = cg.getCalcLogica();
		if (cl.check(1)) {
			try {
				if (!cg.getEditField().equals("")) {
					cl.enter(cg.getEditField());
				}
				Debuggable dtask = (Debuggable) cl.pop();
				cl.setProcDebug(dtask);
				cg.refresh(true);
				cg.temporaryLabels(null, dtask.nextCommand());
			} catch (Exception e) {
				throw new BadArgumentTypeException("Not a program");
			}
		} else {
			throw new TooFewArgumentsException("No program to debug");
		}
	}

}
