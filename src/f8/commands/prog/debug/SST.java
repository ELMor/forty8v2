/*
 * Created on 03-oct-2003
 *
 
 
 */
package f8.commands.prog.debug;

import f8.CL;
import f8.CalcGUI;
import f8.Ref;
import f8.commands.Operation;
import f8.exceptions.F8Exception;
import f8.exceptions.TooFewArgumentsException;

/**
 * @author elinares
 * 
 * 
 * 
 */
public class SST extends Operation {

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Operation#exec()
	 */
	public void exec() throws F8Exception {
		CalcGUI cg = Ref.ref;
		CL cl = cg.getCalcLogica();
		Debuggable dbg = cl.getProcDebug();
		if (dbg != null) {
			dbg.execDebug(false);
			cg.refresh(false);
			if (dbg.isAnyMore()) {
				cg.temporaryLabels(null, dbg.nextCommand());
				cl.setProcDebug(dbg);
			} else {
				cg.temporaryLabels(null, "(returning...)");
			}

		} else {
			throw new TooFewArgumentsException("No program to debug");
		}
	}

}
