/*
 * Created on 03-oct-2003
 *
 
 
 */
package f8.commands.prog.debug;

import f8.exceptions.F8Exception;

/**
 * @author elinares
 * 
 * 
 * 
 */
public interface Debuggable {
	/**
	 * Ejecucion de depuracion. Devuelve false si no se desea incrementar
	 * instruction pointer
	 * 
	 * @return
	 * @throws F8Exception
	 */
	public boolean execDebug(boolean stepInto) throws F8Exception;

	public String nextCommand();

	public boolean isAnyMore();
}
