/*
 * Created on 03-oct-2003
 *
 
 
 */
package f8.commands.prog.debug;

import f8.CL;
import f8.CalcGUI;
import f8.Ref;
import f8.commands.Operation;
import f8.exceptions.F8Exception;
import f8.exceptions.TooFewArgumentsException;

/**
 * @author elinares
 * 
 * 
 * 
 */
public class NEXTSTEP extends Operation {

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Operation#exec()
	 */
	public void exec() throws F8Exception {
		CalcGUI cg = Ref.ref;
		CL cl = cg.getCalcLogica();
		Debuggable dtask = cl.getProcDebug();
		if (dtask != null) {
			cg.temporaryLabels(null, dtask.nextCommand());
			cg.refresh(false);
		} else {
			throw new TooFewArgumentsException("No program to debug");
		}
	}

}
