package f8.commands.prog.brch;

import antlr.collections.AST;
import f8.DataStream;
import f8.Storable;
import f8.commands.Command;
import f8.commands.NonAlgebraic;
import f8.commands.prog.UtilHelper;
import f8.exceptions.F8Exception;

public final class DOUNTIL extends NonAlgebraic {
	Command body;

	Command until;

	public DOUNTIL(AST def) {
		if (def == null) {
			return;
		}

		AST aBody = def.getFirstChild();
		AST aUntil = aBody.getNextSibling();
		body = Command.createFromAST(aBody);
		until = Command.createFromAST(aUntil);
	}

	public int getID() {
		return 42;
	}

	public Storable getInstance() {
		return new DOUNTIL(null);
	}

	public void loadState(DataStream ds) {
		body = Command.loadFromStorage(ds);
		until = Command.loadFromStorage(ds);
	}

	public void saveState(DataStream ds) {
		ds.writeInt(body.getID());
		body.saveState(ds);
		ds.writeInt(until.getID());
		until.saveState(ds);
	}

	public void exec() throws F8Exception {
		do {
			body.exec();
			until.exec();
		} while (UtilHelper.evalCondition() == 0);
	}

	public String toString() {
		if(body==null || until==null ){
			return ("DO  UNTIL 1 END ");
		}
		return ("DO " + body.toString() + " UNTIL " + until.toString() + " END ");
	}
}
