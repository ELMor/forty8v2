package f8.commands.prog.brch;

import antlr.collections.AST;
import f8.DataStream;
import f8.Storable;
import f8.commands.Command;
import f8.commands.NonAlgebraic;
import f8.commands.prog.UtilHelper;
import f8.exceptions.F8Exception;
import f8.kernel.yacc.ObjectParserTokenTypes;

public final class IF extends NonAlgebraic {
	Command cond;

	Command thenBody;

	Command elseBody;

	public IF(AST def) {
		if (def == null) {
			return;
		}

		AST aCond = def.getFirstChild();
		AST aThen = aCond.getNextSibling();
		AST aElse = aThen.getNextSibling();
		cond = Command.createFromAST(aCond);
		thenBody = Command.createFromAST(aThen);

		if (def.getType() == ObjectParserTokenTypes.IFELSE) {
			elseBody = Command.createFromAST(aElse);
		}
	}

	public int getID() {
		return 23;
	}

	public Storable getInstance() {
		return new IF(null);
	}

	public void loadState(DataStream ds) {
		cond = Command.loadFromStorage(ds);
		thenBody = Command.loadFromStorage(ds);

		int tipo = ds.readInt();

		if (tipo == 1) {
			elseBody = Command.loadFromStorage(ds);
		} else {
			elseBody = null;
		}
	}

	public void saveState(DataStream ds) {
		ds.writeInt(cond.getID());
		cond.saveState(ds);

		ds.writeInt(thenBody.getID());
		thenBody.saveState(ds);

		ds.writeInt((elseBody == null) ? 0 : 1);

		if (elseBody != null) {
			ds.writeInt(elseBody.getID());
			elseBody.saveState(ds);
		}
	}

	public void exec() throws F8Exception {
		cond.exec();

		int res = UtilHelper.evalCondition();

		if (res == 1) {
			thenBody.exec();
		}

		if ((res == 0) && (elseBody != null)) {
			elseBody.exec();
		}
	}

	public String toString() {
		if(cond==null || thenBody==null){
			return "IF 0 THEN  END";
		}
		return "IF " + cond.toString() + " THEN " + thenBody.toString()
				+ ((elseBody == null) ? "" : (" ELSE " + elseBody.toString()))
				+ " END";
	}
}
