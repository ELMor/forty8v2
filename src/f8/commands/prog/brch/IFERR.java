package f8.commands.prog.brch;

import antlr.collections.AST;
import f8.DataStream;
import f8.Storable;
import f8.commands.Command;
import f8.commands.NonAlgebraic;
import f8.exceptions.F8Exception;

public final class IFERR extends NonAlgebraic {
	Command cond;

	Command thenBody;

	public IFERR(AST def) {
		if (def == null) {
			return;
		}

		AST aCond = def.getFirstChild();
		AST aThen = aCond.getNextSibling();
		cond = Command.createFromAST(aCond);
		thenBody = Command.createFromAST(aThen);

	}

	public int getID() {
		return 499;
	}

	public Storable getInstance() {
		return new IFERR(null);
	}

	public void loadState(DataStream ds) {
		cond = Command.loadFromStorage(ds);
		thenBody = Command.loadFromStorage(ds);
	}

	public void saveState(DataStream ds) {
		ds.writeInt(cond.getID());
		cond.saveState(ds);
		ds.writeInt(thenBody.getID());
		thenBody.saveState(ds);

	}

	public void exec() throws F8Exception {
		try {
			cond.exec();
		} catch (F8Exception e) {
			thenBody.exec();
		}

	}

	public String toString() {
		return "IFERR " + cond.toString() + " THEN " + thenBody.toString()
				+ " END";
	}
}
