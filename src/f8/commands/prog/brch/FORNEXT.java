/*
 * Created on 17-ago-2003
 *
 
 
 */
package f8.commands.prog.brch;

import antlr.collections.AST;
import f8.CL;
import f8.DataStream;
import f8.Hashtable;
import f8.Ref;
import f8.Storable;
import f8.commands.Command;
import f8.commands.NonAlgebraic;
import f8.commands.prog.UtilHelper;
import f8.exceptions.F8Exception;
import f8.kernel.yacc.ObjectParserTokenTypes;
import f8.objects.Stackable;
import f8.objects.types.Double;

/**
 * @author elinares
 * 
 * 
 * 
 */
public final class FORNEXT extends NonAlgebraic {
	Command var;

	Command body;

	Command incr;

	public FORNEXT(AST fn) {
		super();

		if (fn == null) {
			return;
		}

		AST ch1 = fn.getFirstChild();
		AST ch2 = ch1.getNextSibling();
		var = Command.createFromAST(ch1);
		body = Command.createFromAST(ch2);

		if (fn.getType() == ObjectParserTokenTypes.FORNEXT) {
			incr = new Double(1);
		} else {
			incr = Command.createFromAST(ch2.getNextSibling());
		}
	}

	public void exec() throws F8Exception {
		CL cl = Ref.ref.getCalcLogica();
		Command tope = cl.pop();
		Command inic = cl.pop();
		String vname = var.toString();
		cl.pushLocalEnvir();
		Hashtable ht = cl.getCurrentLocalEnvir();
		ht.put(vname, inic);

		do {
			body.exec();

			cl.push((Stackable) cl.lookup(vname));
			incr.exec();
			cl.lookup("+").exec();
			inic = cl.peek();
			ht.put(vname, inic);
			tope.exec();
			cl.lookup("<=").exec();
		} while (UtilHelper.evalCondition() == 1);
		cl.popLocalEnvir();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.pfr.io.Storable#getID()
	 */
	public int getID() {
		return 117;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.pfr.io.Storable#getInstance()
	 */
	public Storable getInstance() {
		return new FORNEXT(null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.pfr.io.Storable#saveState(f8.pfr.io.DataStream)
	 */
	public void saveState(DataStream ds) {
		ds.writeInt(var.getID());
		var.saveState(ds);

		ds.writeInt(body.getID());
		body.saveState(ds);

		ds.writeInt(incr.getID());
		incr.saveState(ds);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.pfr.io.Storable#loadState(f8.pfr.io.DataStream)
	 */
	public void loadState(DataStream ds) {
		var = (Command) Command.loadFromStorage(ds);
		body = (Command) Command.loadFromStorage(ds);
		incr = (Command) Command.loadFromStorage(ds);
	}

	public String toString() {
		if(var==null||body==null)
			return "FOR i NEXT";
		return "FOR " + var + " " + body.toString() + " " + incr.toString()
				+ " STEP";
	}
}
