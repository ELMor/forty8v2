/*
 * Created on 03-oct-2003
 *
 
 
 */
package f8.commands.prog;

import antlr.collections.AST;
import f8.CL;
import f8.DataStream;
import f8.Hashtable;
import f8.Ref;
import f8.Storable;
import f8.UtilFactory;
import f8.Vector;
import f8.commands.Command;
import f8.commands.prog.debug.Debuggable;
import f8.exceptions.F8Exception;
import f8.exceptions.TooFewArgumentsException;
import f8.objects.Stackable;
import f8.objects.types.Proc;

/**
 * @author elinares
 * 
 * 
 * 
 */
public class FLECHA extends Command implements Debuggable {
	Vector locals = UtilFactory.newVector();

	Stackable domain = null;

	boolean localsLoaded = false;

	public FLECHA(AST def) {
		if (def == null) {
			return;
		}
		int nv = def.getNumberOfChildren() - 1;
		AST child = def.getFirstChild();
		for (int i = 0; i < nv; i++) {
			locals.add(child.getText());
			child = child.getNextSibling();
		}
		domain = (Stackable) Command.createFromAST(child);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.Storable#getID()
	 */
	public int getID() {
		return 901;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.Storable#getInstance()
	 */
	public Storable getInstance() {
		return new FLECHA(null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.Storable#loadState(f8.platform.DataStream)
	 */
	public void loadState(DataStream ds) {
		int sz = ds.readInt();
		for (int i = 0; i < sz; i++) {
			locals.add(ds.readStringSafe());
		}
		domain = (Stackable) Command.loadFromStorage(ds);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.Storable#saveState(f8.platform.DataStream)
	 */
	public void saveState(DataStream ds) {
		ds.writeInt(locals.size());
		for (int i = 0; i < locals.size(); i++) {
			ds.writeStringSafe((String) locals.elementAt(i));
		}
		ds.writeInt(domain.getID());
		domain.saveState(ds);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Command#isInfix()
	 */
	public boolean isInfix() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Command#isOperator()
	 */
	public boolean isOperator() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Operation#exec()
	 */
	public void exec() throws F8Exception {
		CL cl = Ref.ref.getCalcLogica();
		loadLocals(cl);
		try {
			domain.exec();
		} catch (F8Exception e) {
			throw e;
		} finally {
			cl.popLocalEnvir();
		}
	}

	private void loadLocals(CL cl) throws TooFewArgumentsException {
		cl.pushLocalEnvir();
		Hashtable ht = cl.getCurrentLocalEnvir();
		int sz = locals.size();
		if (cl.check(sz)) {
			while (--sz >= 0) {
				ht.put(locals.elementAt(sz), cl.pop());
			}
			localsLoaded = true;
		} else {
			throw new TooFewArgumentsException(this);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.types.utils.Debuggable#execDebug()
	 */
	public boolean execDebug(boolean si) throws F8Exception {
		CL cl = Ref.ref.getCalcLogica();
		if (!localsLoaded) {
			loadLocals(cl);
			return false;
		} else {
			try {
				Debuggable dtask = (Debuggable) domain;
				return dtask.execDebug(si);
			} catch (ClassCastException e) {
				domain.exec();
				return false;
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.types.utils.Debuggable#nextCommand()
	 */
	public String nextCommand() {
		if (!localsLoaded)
			return toString();
		Debuggable dtask;
		try {
			dtask = (Debuggable) domain;
			return dtask.nextCommand();
		} catch (ClassCastException e) {
			return domain.toString();
		}
	}

	public int size() {
		return locals.size();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		String ret = "\u008D";
		for (int i = 0; i < locals.size(); i++) {
			ret += (" " + locals.elementAt(i));
		}
		if (domain != null) {
			ret += (" " + domain.toString());
		}
		return ret;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.types.utils.Debuggable#isAnyMore()
	 */
	public boolean isAnyMore() {
		if (domain instanceof Proc) {
			return ((Proc) domain).isAnyMore();
		}
		return localsLoaded == false;
	}

}
