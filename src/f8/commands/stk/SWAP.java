package f8.commands.stk;

import f8.CL;
import f8.DataStream;
import f8.Ref;
import f8.Storable;
import f8.commands.Command;
import f8.commands.NonAlgebraic;
import f8.exceptions.F8Exception;
import f8.exceptions.TooFewArgumentsException;

public final class SWAP extends NonAlgebraic {
	public SWAP() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 49;
	}

	public Storable getInstance() {
		return new SWAP();
	}

	public void loadState(DataStream ds) {
	}

	public void saveState(DataStream ds) {
	}

	public void exec() throws F8Exception {
		CL cl = Ref.ref.getCalcLogica();
		if (cl.check(2)) {
			Command a = cl.peek(0);
			Command b = cl.peek(1);
			cl.poke(1, a);
			cl.poke(0, b);
		} else {
			throw new TooFewArgumentsException(this);
		}
	}

	public String toString() {
		return ("SWAP");
	}
}
