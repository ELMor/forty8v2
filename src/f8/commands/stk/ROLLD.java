package f8.commands.stk;

import f8.CL;
import f8.DataStream;
import f8.Ref;
import f8.Storable;
import f8.UtilFactory;
import f8.Vector;
import f8.commands.Command;
import f8.commands.NonAlgebraic;
import f8.exceptions.F8Exception;
import f8.exceptions.TooFewArgumentsException;
import f8.objects.Stackable;
import f8.objects.types.Double;

public final class ROLLD extends NonAlgebraic {
	public ROLLD() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 11;
	}

	public Storable getInstance() {
		return new ROLLD();
	}

	public void loadState(DataStream ds) {
	}

	public void saveState(DataStream ds) {
	}

	public void exec() throws F8Exception {
		CL cl = Ref.ref.getCalcLogica();
		int rot;
		if (cl.check(1)) {
			Command a = cl.pop();
			if (a instanceof Double) {
				rot = (int) ((Double) a).x;
				if (cl.check(rot)) {
					Vector gb = UtilFactory.newVector();
					for (int i = 0; i < rot; i++) {
						gb.add(cl.pop());
					}
					cl.push((Stackable) gb.elementAt(rot - 1));
					for (int i = 0; i < (rot - 1); i++) {
						cl.push((Stackable) gb.elementAt(i));
					}
				} else {
					throw new TooFewArgumentsException(this);
				}
			}
		} else {
			throw new TooFewArgumentsException(this);
		}
	}

	public String toString() {
		return ("ROLLD");
	}
}
