package f8.commands.stk;

import f8.CL;
import f8.DataStream;
import f8.Ref;
import f8.Storable;
import f8.commands.NonAlgebraic;
import f8.objects.types.Double;

public final class DEPTH extends NonAlgebraic {
	public DEPTH() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 17;
	}

	public Storable getInstance() {
		return new DEPTH();
	}

	public void loadState(DataStream ds) {
	}

	public void saveState(DataStream ds) {
	}

	public void exec() {
		CL cl = Ref.ref.getCalcLogica();
		cl.push(new Double(cl.size()));
	}

	public String toString() {
		return ("DEPTH");
	}
}
