package f8.commands.stk;

import f8.CL;
import f8.DataStream;
import f8.Ref;
import f8.Storable;
import f8.commands.Command;
import f8.commands.NonAlgebraic;
import f8.exceptions.BadArgumentTypeException;
import f8.exceptions.F8Exception;
import f8.exceptions.TooFewArgumentsException;
import f8.objects.types.Double;

public final class DUPN extends NonAlgebraic {
	public DUPN() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 19;
	}

	public Storable getInstance() {
		return new DUPN();
	}

	public void loadState(DataStream ds) {
	}

	public void saveState(DataStream ds) {
	}

	public void exec() throws F8Exception {
		CL cl = Ref.ref.getCalcLogica();
		if (cl.check(1)) {
			Command a = cl.pop();
			if (a instanceof Double) {
				int rot = (int) ((Double) a).x;
				if (cl.check(rot)) {
					for (int i = 0; i < rot; i++) {
						cl.push(cl.peek(rot - 1));
					}
				} else {
					throw new TooFewArgumentsException(this);
				}
			} else {
				throw new BadArgumentTypeException(this);
			}
		} else {
			throw new TooFewArgumentsException(this);
		}
	}

	public String toString() {
		return ("DUPN");
	}
}
