package f8.commands.stk;

import f8.CL;
import f8.DataStream;
import f8.Ref;
import f8.Storable;
import f8.commands.NonAlgebraic;
import f8.exceptions.F8Exception;
import f8.exceptions.TooFewArgumentsException;

public final class DUP extends NonAlgebraic {
	public DUP() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 9;
	}

	public Storable getInstance() {
		return new DUP();
	}

	public void loadState(DataStream ds) {
	}

	public void saveState(DataStream ds) {
	}

	public void exec() throws F8Exception {
		CL cl = Ref.ref.getCalcLogica();
		if (cl.check(1)) {
			cl.push(cl.peek());
		} else {
			throw new TooFewArgumentsException(this);
		}
	}

	public String toString() {
		return ("DUP");
	}
}
