package f8.commands.storage;

import f8.CL;
import f8.DataStream;
import f8.Ref;
import f8.Storable;
import f8.commands.Command;
import f8.commands.NonAlgebraic;
import f8.exceptions.BadArgumentTypeException;
import f8.exceptions.F8Exception;
import f8.exceptions.TooFewArgumentsException;
import f8.keyboard.Menu;
import f8.objects.Stackable;
import f8.objects.types.Lista;
import f8.objects.types.Literal;

public final class MENU extends NonAlgebraic {
	public MENU() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 436;
	}

	public Storable getInstance() {
		return new MENU();
	}

	public void loadState(DataStream ds) {
	}

	public void saveState(DataStream ds) {
	}

	public void exec() throws F8Exception {
		CL cl = Ref.ref.getCalcLogica();

		if (cl.check(1)) {
			Command a = cl.peek(0);
			if (a instanceof Lista) {
				cl.push(new Literal("CST"));
				new STO().exec();
				showCustomMenu();
			} else {
				throw new BadArgumentTypeException(this);
			}
		} else {
			throw new TooFewArgumentsException(this);
		}
	}

	public String toString() {
		return ("MENU");
	}

	public static void showCustomMenu() throws F8Exception {
		CL cl = Ref.ref.getCalcLogica();
		Stackable cst = (Stackable) cl.lookup("CST");
		if (cst instanceof Lista) {
			Menu.createAndDisplayCustomMenu((Lista) cst);
		} else {
			throw new BadArgumentTypeException(new MENU());
		}
	}
}
