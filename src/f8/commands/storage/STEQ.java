package f8.commands.storage;

import f8.CL;
import f8.DataStream;
import f8.Ref;
import f8.Storable;
import f8.commands.Command;
import f8.commands.NonAlgebraic;
import f8.exceptions.F8Exception;
import f8.exceptions.TooFewArgumentsException;

public final class STEQ extends NonAlgebraic {
	public STEQ() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 2094;
	}

	public Storable getInstance() {
		return new STEQ();
	}

	public void loadState(DataStream ds) {
	}

	public void saveState(DataStream ds) {
	}

	public void exec() throws F8Exception {
		CL cl = Ref.ref.getCalcLogica();

		if (cl.check(1)) {
			Command b = cl.pop();
			String name;
			name = "EQ";
			STO.storeVar(name, b);
		} else {
			throw new TooFewArgumentsException(this);
		}
	}

	public String toString() {
		return ("STEQ");
	}
}
