package f8.commands.storage;

import f8.CL;
import f8.DataStream;
import f8.Hashtable;
import f8.Ref;
import f8.Storable;
import f8.commands.Command;
import f8.commands.NonAlgebraic;
import f8.exceptions.BadArgumentTypeException;
import f8.exceptions.F8Exception;
import f8.exceptions.TooFewArgumentsException;
import f8.objects.Stackable;
import f8.objects.types.Directory;
import f8.objects.types.InfixExp;
import f8.objects.types.Literal;

public final class PGDIR extends NonAlgebraic {
	public PGDIR() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 509;
	}

	public Storable getInstance() {
		return new PGDIR();
	}

	public void loadState(DataStream ds) {
	}

	public void saveState(DataStream ds) {
	}

	public void exec() throws F8Exception {
		CL cl = Ref.ref.getCalcLogica();

		if (cl.check(1)) {
			Command a = cl.peek(0);
			String name;
			Hashtable itemHolder = cl.currentDict().getHT();
			if (a instanceof Literal) {
				name = ((Literal) a).nombre;
				removeDir(cl, itemHolder, name);
			} else if (a instanceof InfixExp
					&& ((name = InfixExp.isLiteral(a)) != null)) {
				removeDir(cl, itemHolder, name);
			} else {
				throw new BadArgumentTypeException(this);
			}
		} else {
			throw new TooFewArgumentsException(this);
		}
	}

	private void removeDir(CL cl, Hashtable itemHolder, String name)
			throws F8Exception {
		cl.pop();
		if ((Stackable) itemHolder.get(name) instanceof Directory) {
			itemHolder.remove(name);
		} else {
			throw new BadArgumentTypeException(this);
		}
		Ref.ref.refreshProgMenu();
	}

	public String toString() {
		return ("PGDIR");
	}
}
