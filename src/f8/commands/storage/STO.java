package f8.commands.storage;

import f8.CL;
import f8.DataStream;
import f8.Hashtable;
import f8.Ref;
import f8.Storable;
import f8.Vector;
import f8.commands.Command;
import f8.commands.NonAlgebraic;
import f8.commands.math.mkl;
import f8.exceptions.BadArgumentTypeException;
import f8.exceptions.CircularReferenceException;
import f8.exceptions.F8Exception;
import f8.exceptions.TooFewArgumentsException;
import f8.objects.Stackable;
import f8.objects.types.InfixExp;
import f8.objects.types.Literal;

public final class STO extends NonAlgebraic {
	public STO() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 5;
	}

	public Storable getInstance() {
		return new STO();
	}

	public void loadState(DataStream ds) {
	}

	public void saveState(DataStream ds) {
	}

	public void exec() throws F8Exception {
		CL cl = Ref.ref.getCalcLogica();

		if (cl.check(2)) {
			Command a = cl.peek(0);
			Command b = cl.peek(1);
			String name;
			if (a instanceof Literal) {
				name = ((Literal) a).nombre;
				cl.pop(2);
				storeVar(name, b);
			} else if (a instanceof InfixExp
					&& ((name = InfixExp.isLiteral(a)) != null)) {
				cl.pop(2);
				storeVar(name, b);
			} else {
				throw new BadArgumentTypeException(this);
			}
		} else {
			throw new TooFewArgumentsException(this);
		}
	}

	public static void storeVar(String name, Command b)
			throws CircularReferenceException {
		Stackable j;
		CL cl = Ref.ref.getCalcLogica();
		try {
			j = (Stackable) b;
			if (j instanceof InfixExp) {
				Vector incog = mkl.incogOf(cl, j.getAST());
				if (incog.find(name) >= 0) {
					throw new CircularReferenceException(new STO());
				}
			}
		} catch (ClassCastException cce) {
		}
		Hashtable h = cl.currentDict().getHT();
		h.put(name, b);
		Ref.ref.refreshProgMenu();
	}

	public String toString() {
		return ("STO");
	}
}
