package f8.commands.math.base;

import f8.DataStream;
import f8.Storable;

public final class AND extends Logical2 {
	public AND() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 1;
	}

	public Storable getInstance() {
		return new AND();
	}

	public void loadState(DataStream ds) {
	}

	public void saveState(DataStream ds) {
	}

	public String toString() {
		return ("AND");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch2#prfDoubleDouble(f8.kernel.types.Double,
	 *      f8.kernel.types.Double)
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.op.boo.Logical#logic(double, double)
	 */
	public double logic(double l, double r) {
		return l * r == 0 ? 0 : 1;
	}

}
