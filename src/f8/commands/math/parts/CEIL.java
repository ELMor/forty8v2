package f8.commands.math.parts;

import f8.DataStream;
import f8.Storable;
import f8.commands.Dispatch1;
import f8.exceptions.F8Exception;
import f8.objects.Stackable;
import f8.objects.types.Double;

public final class CEIL extends Dispatch1 {
	public CEIL() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 3014;
	}

	public String toString() {
		return ("CEIL");
	}

	public Storable getInstance() {
		return new CEIL();
	}

	public void loadState(DataStream ds) {
	}

	public void saveState(DataStream ds) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch1#prfDouble(f8.kernel.types.Double)
	 */
	public Stackable prfDouble(Double a) throws F8Exception {
		double x = a.doubleValue();
		int m = (int) Math.floor(x);
		if (m > x) {
			m--;
		}
		return (new Double(m + 1));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return true;
	}

}
