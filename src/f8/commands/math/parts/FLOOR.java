package f8.commands.math.parts;

import f8.DataStream;
import f8.Storable;
import f8.commands.Dispatch1;
import f8.exceptions.F8Exception;
import f8.objects.Stackable;
import f8.objects.types.Double;
import f8.objects.types.Int;

public final class FLOOR extends Dispatch1 {
	public FLOOR() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 18;
	}

	public Storable getInstance() {
		return new FLOOR();
	}

	public void loadState(DataStream ds) {
	}

	public void saveState(DataStream ds) {
	}

	public String toString() {
		return ("FLOOR");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch1#prfDouble(f8.kernel.types.Double)
	 */
	public Stackable prfDouble(Double a) throws F8Exception {
		double x = a.doubleValue();
		int m = (int) Math.floor(x);
		if (m > x) {
			m--;
		}
		return (new Double(m));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch1#prfInt(f8.kernel.types.Int)
	 */
	public Stackable prfInt(Int x) throws F8Exception {
		return x;
	}

}
