package f8.commands.math.parts;

import f8.DataStream;
import f8.Storable;
import f8.commands.Dispatch2;
import f8.exceptions.F8Exception;
import f8.objects.Stackable;
import f8.objects.types.Double;

public final class TRNC extends Dispatch2 {
	public TRNC() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 3016;
	}

	public String toString() {
		return ("TRNC");
	}

	public Storable getInstance() {
		return new TRNC();
	}

	public void loadState(DataStream ds) {
	}

	public void saveState(DataStream ds) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Dispatch2#prfDoubleDouble(f8.types.Double, f8.types.Double)
	 */
	public Stackable prfDoubleDouble(Double a, Double b) throws F8Exception {
		int pos = (int) b.x;
		int mant = (int) (a.x * Math.pow(10, pos));
		return new Double(mant / Math.pow(10, pos));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Command#isInfix()
	 */
	public boolean isInfix() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Command#isOperator()
	 */
	public boolean isOperator() {
		return false;
	}

}
