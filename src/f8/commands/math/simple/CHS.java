package f8.commands.math.simple;

import f8.DataStream;
import f8.Storable;
import f8.commands.Dispatch1;
import f8.commands.math.mkl;
import f8.exceptions.F8Exception;
import f8.objects.Stackable;
import f8.objects.types.Complex;
import f8.objects.types.Double;
import f8.objects.types.F8Vector;
import f8.objects.types.InfixExp;
import f8.objects.types.Int;

public final class CHS extends Dispatch1 {
	public CHS() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 801;
	}

	public Storable getInstance() {
		return new CHS();
	}

	public void loadState(DataStream ds) {
	}

	public void saveState(DataStream ds) {
	}

	public String toString() {
		return ("CHS");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch1#prfInt(f8.kernel.types.Int)
	 */
	public Stackable prfInt(Int a) throws F8Exception {
		int A = a.intValue();
		A = -A;
		return new Int(A);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch1#prfDouble(f8.kernel.types.Double)
	 */
	public Stackable prfDouble(Double a) throws F8Exception {
		double x = a.doubleValue();
		x = -x;
		return new Double(x);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch1#prfComplex(f8.kernel.types.Complex)
	 */
	public Stackable prfComplex(Complex a) throws F8Exception {
		double[] x = a.complexValue();
		return new Complex(-x[0], -x[1]);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch1#prfVector(f8.kernel.types.F8Vector)
	 */
	public Stackable prfVector(F8Vector a) throws F8Exception {
		for (int i = 0; i < a.x.length; i++)
			a.x[i] = -a.x[i];
		return new F8Vector(a.x);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch1#prfInfix(f8.kernel.types.InfixExp)
	 */
	public Stackable prfInfix(InfixExp a) throws F8Exception {
		return new InfixExp(mkl.neg(a.getAST()));
	}

}
