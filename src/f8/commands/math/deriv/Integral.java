/*
 * Created on 17-ago-2003
 *
 
 
 */
package f8.commands.math.deriv;

import antlr.CommonAST;
import antlr.collections.AST;
import f8.CL;
import f8.DataStream;
import f8.Ref;
import f8.Storable;
import f8.commands.Command;
import f8.exceptions.BadArgumentTypeException;
import f8.exceptions.F8Exception;
import f8.exceptions.TooFewArgumentsException;
import f8.objects.Stackable;
import f8.objects.types.Double;
import f8.objects.types.InfixExp;
import f8.objects.types.Literal;

/**
 * @author elinares
 * 
 * 
 * 
 */
public final class Integral extends Command {
	AST full;

	public Integral(AST tree) {
		full = tree;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return true;
	}

	public void exec() throws F8Exception {
		CL cl = Ref.ref.getCalcLogica();
		if (cl.check(4)) {
			Stackable from = cl.peek(3);
			Stackable to = cl.peek(2);
			Stackable exp = cl.peek(1);
			Stackable var = cl.peek(0);
			String varName = null;
			if ((var instanceof Literal || ((varName = InfixExp.isLiteral(var)) != null))
					&& (from instanceof Double || from instanceof InfixExp)
					&& (to instanceof Double || to instanceof InfixExp)
					&& (exp instanceof Double || exp instanceof InfixExp)) {
				cl.pop(4);

				AST integral = new CommonAST();
				integral.setText("\u0084");
				integral.setType(INTEG);
				integral.addChild(from.getAST());
				integral.addChild(to.getAST());
				integral.addChild(exp.getAST());
				integral.addChild(var.getAST());
				cl.push(new InfixExp(integral));
			} else {
				throw new BadArgumentTypeException(this);
			}
		} else {
			throw new TooFewArgumentsException(this);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.pfr.io.Storable#getID()
	 */
	public int getID() {
		return 1118;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.pfr.io.Storable#getInstance()
	 */
	public Storable getInstance() {
		return new Integral(null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.pfr.io.Storable#saveState(f8.pfr.io.DataStream)
	 */
	public void saveState(DataStream ds) {
		Command.saveState(ds, full);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.pfr.io.Storable#loadState(f8.pfr.io.DataStream)
	 */
	public void loadState(DataStream ds) {
		AST[] nodes = new AST[1];
		Command.loadState(ds, nodes);
		full = nodes[0];
	}

	public String toString() {
		return "\u0084";
	}

	public static String forma(AST sigma) {
		AST child = sigma.getFirstChild();
		String sigId = InfixExp.forma(child);
		child = child.getNextSibling();
		String sigDo = InfixExp.forma(child);
		child = child.getNextSibling();
		String sigUp = InfixExp.forma(child);
		child = child.getNextSibling();
		String sigFu = InfixExp.forma(child);
		return "\u0084(" + sigId + "=" + sigDo + "," + sigUp + "," + sigFu
				+ ")";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isInfix()
	 */
	public boolean isInfix() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isOperator()
	 */
	public boolean isOperator() {
		// TODO Auto-generated method stub
		return false;
	}

}
